# Life Trading C++ Coding Exercise

In this repository, there is a single header file (IExchange.hpp).

The aim of this task is to provide a working implementation for the interface class in C++. The interface is designed to represent a simplified exchange. It includes the ability for clients to add and remove orders from the orderbook(s), plus the ability to be notified when an order trades or the best bid/offer has changed.

## Details
- Solution should support multiple instruments (each identified by a string identifier)
- Keep in mind the type of containers being used to store data.
- Treat this as if it were production code
    * How would you ensure the correctness of the code?
    * Treat it as your best code, not fastest written code
- Feel free to use c++14/17, STL, Boost, GoogleTest or whatever publicly available tools/libraries required
- Include a CMake/Makefile so we can build you implementation and run any unit tests you include
- Don't worry about including a UI or command line interface - the unit tests will suffice for driving the implementation
- Code can be written to run on Windows or Linux
