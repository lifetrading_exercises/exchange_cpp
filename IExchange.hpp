#pragma once

#include <functional>
#include <string>
#include <utility>

enum Side
{
    BUY,
    SELL
};

// Example exchange interface
class IExchange
{
public:
    virtual ~IExchange() = default;

    // Add an order to the exchange
    // @return unique identifier for the order, -ive value indicates failure
    //         +ive value indicates success
    virtual int AddOrder(
        const std::string& instrument,
        Side side,
        int price,
        std::uint32_t quantity) = 0;

    // Remove an order from the exchange
    // @param the order id to remove, returned from AddOrder
    // @return success or failure
    virtual bool RemoveOrder(int exchangeOrderId) = 0;

    // callback to indicate an order has been matched
    // @tradedPrice the price of the order that has traded
    // @tradedQuantity the quantity of the order that has traded
    std::function<void(
        int orderId, 
        int tradedPrice, 
        std::uint32_t tradedQuantity)>
        OrderTraded{};

    // callback to indicate the best price has changed for a given instrument
    // @param bidTotalQuantity - total quantity of all orders at the bid price
    // @param askTotalQuantity - total quantity of all orders at the ask price
    std::function<void(
        const std::string& instrument,
        int bidPrice,
        std::uint32_t bidTotalQuantity,
        int askPrice,
        std::uint32_t askTotalQuantity)>
        BestPriceChanged{};
};